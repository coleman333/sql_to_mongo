const async = require('async');
const util = require('util');

module.exports = {
	each: util.promisify(async.each),
	map: util.promisify(async.map),
};
