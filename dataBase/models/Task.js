const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const taskSchema = new Schema({
	title: {type: String, required: true},
	deadLine: {type: Date},
	dateTask: {type: Date},                            //  (the date the best for this task)
	periodHours: {type: Number},             //(the time the best for this task)
	periodMinutes: {type: Number},             // (the time period for this task)
	readyOrNot: {type: Boolean, default: false},
	userId: {type: Schema.ObjectId, ref: 'User', required: true}

});

module.exports = mongoose.model('Task', taskSchema);