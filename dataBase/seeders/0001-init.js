const _ = require('lodash');
const async = require('../../server/utils/promisified-async');
const mongoose = require('mongoose');
const models = require('../models');
const {host, port, database} = require('../config/config.json')['development'];
const Sequelize = require('sequelize');
const sequelize = new Sequelize('postgres://postgres:root@localhost:5432/migrations');

mongoose.connect(`mongodb://${host}:${port}/${database}`);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
	// we're connected!
	console.log(`we're connected!`)
});

// const object = {a:1,b:2,c:'hello',d:{q:1,w:2}};
// const {a:a1,b,c,d:{q:q1}={}}=object;

// const a1=object.a;
// const b=object.b;
// const c=object.c;
// const q1=object.d.q;

(async () => {
	try {
		// language=GenericSQL
		const users = await sequelize.query(`
			SELECT
				"Users".*,
				json_agg("Tasks".*) AS tasks
			FROM "Users"
				LEFT JOIN "Tasks" ON "Users".id = "Tasks"."userId"
			GROUP BY "Users".id;
		`, {
			type: Sequelize.QueryTypes.SELECT
		});

		await async.each(
			users,
			async user => {
				const newUser = await models.User.create(_.omit(user, ['id', 'tasks']));

				const userTasks = await async.map(
					_.compact(user.tasks),
					async task => _.chain(task).omit('id').set('userId', newUser._id).value()
				);
				if (_.isEmpty(userTasks)) {
					return;
				}
				return models.Task.collection.insert(userTasks);
			}
		);

		console.log(`\n\n=>`, 'done');
	} catch (ex) {
		console.log(ex)
	}

	process.exit();
})();
